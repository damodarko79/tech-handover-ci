# Tech Handover CI

A sample project to test Continuous Integration with GitLab CI.

[![pipeline status](https://gitlab.com/damodarko79/tech-handover-ci/badges/master/pipeline.svg)](https://gitlab.com/damodarko79/tech-handover-ci/commits/master)

[![coverage report](https://gitlab.com/damodarko79/tech-handover-ci/badges/master/coverage.svg)](https://gitlab.com/damodarko79/tech-handover-ci/commits/master)

### Installation
```sh
$ git clone https://gitlab.com/damodarko79/tech-handover-ci.git tech-handover-ci
$ cd tech-handover-ci
$ mvn clean install
```
