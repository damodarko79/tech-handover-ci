package com.github.damianmcdonald.ci;

public class PhilosophyService {

    public String getPhilosophyByPhilophoser(Philosophies philosopher) {
        String philosophy;
        switch (philosopher) {
            case SOCRATES:
                philosophy = Philosophies.SOCRATES.toString();
                break;
            case DESCARTES:
                philosophy = Philosophies.DESCARTES.toString();
                break;
            case HERACLITUS:
                philosophy = Philosophies.HERACLITUS.toString();
                break;
            case VOLTAIRE:
                philosophy = Philosophies.VOLTAIRE.toString();
                break;
            case ARISTOTLE:
                philosophy = Philosophies.ARISTOTLE.toString();
                break;
            default:
                throw new IllegalArgumentException(String.format("%s is not a valid Philophoser", philosopher.name()));
        }
        return philosophy;
    }

}
