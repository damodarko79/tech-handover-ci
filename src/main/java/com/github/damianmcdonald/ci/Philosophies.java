package com.github.damianmcdonald.ci;

public enum Philosophies {

    SOCRATES("The unexamined life is not worth living"),
    DESCARTES("I think therefore I am\\” (\\“Cogito, ergo sum\\”)"),
    HERACLITUS("One cannot step twice in the same river"),
    VOLTAIRE("If God did not exist, it would be necessary to invent Him"),
    ARISTOTLE("We are what we repeatedly do. Excellence, then, is not an act, but a habit"),
    PLATO("You can discover more about a person in an hour of play than in a year of conversation");

    private final String philosphy;

    Philosophies(final String philosphy) {
        this.philosphy = philosphy;
    }

    @Override
    public String toString() {
        return philosphy;
    }
}
