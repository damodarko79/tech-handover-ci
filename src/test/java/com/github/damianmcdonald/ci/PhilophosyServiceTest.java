package com.github.damianmcdonald.ci;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PhilophosyServiceTest {

    private PhilosophyService service;

    @Before
    public void testSetUp() {
        service = new PhilosophyService();
    }

    @Test
    public void testGetPhilosophyByPhilophoser() {
        assertEquals("The unexamined life is not worth living", service.getPhilosophyByPhilophoser(Philosophies.SOCRATES));
        assertEquals("I think therefore I am\\” (\\“Cogito, ergo sum\\”)", service.getPhilosophyByPhilophoser(Philosophies.DESCARTES));
        assertEquals("One cannot step twice in the same river", service.getPhilosophyByPhilophoser(Philosophies.HERACLITUS));
        assertEquals("If God did not exist, it would be necessary to invent Him", service.getPhilosophyByPhilophoser(Philosophies.VOLTAIRE));
        assertEquals("We are what we repeatedly do. Excellence, then, is not an act, but a habit", service.getPhilosophyByPhilophoser(Philosophies.ARISTOTLE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDossierKindIllegalArgumentException() {
        service.getPhilosophyByPhilophoser(Philosophies.PLATO);
    }

}
